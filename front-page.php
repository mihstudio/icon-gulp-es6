<?php

get_header();

if( have_rows('sections') ):
    while ( have_rows('sections') ) : the_row();

        // Case: Paragraph layout.
        if( get_row_layout() === 'hero' ):
            get_template_part('template-parts/modules/hero');

        elseif( get_row_layout() === 'boxs' ):
            get_template_part('template-parts/modules/boxs');

        elseif( get_row_layout() === 'movie' ):
            get_template_part('template-parts/modules/movie');

        elseif( get_row_layout() === 'features' ):
            get_template_part('template-parts/modules/features');

        elseif( get_row_layout() === 'realization' ):
            get_template_part('template-parts/modules/realization');
            get_template_part('template-parts/widgets/partners');

        elseif( get_row_layout() === 'blog' ):
            get_template_part('template-parts/modules/blog');

        elseif( get_row_layout() === 'about' ):
            get_template_part('template-parts/modules/section-about');


        endif;
    endwhile;

else :
endif;

get_footer();