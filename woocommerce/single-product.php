<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if (is_shop()) {
    $page_id = wc_get_page_id('shop');
} else {
    $page_id = get_option( 'woocommerce_shop_page_id' );
}



get_header();

$custom_hero_bg = get_field('custom_hero_bg', $page_id);
$dif_title = get_field('different_title', $page_id);
$extra_link = get_field('extra_link', $page_id);
?>

    <section class="hero hero-center lazy" data-bg="<?php
    echo $custom_hero_bg['url'] ?>">
        <div class="container">
            <div class="row align-items-end">
                <div class="col-12 text-center">
                    <h2 class="section-title text-center">
                        <?php
                            the_title();
                        ?>
                    </h2>
                    <?php
                    if ($extra_link) : ?>
                        <div class="hero__link">
                            <a class="btn btn-white" href="<?= $extra_link['url']; ?>"
                               target="<?= $extra_link['target']; ?>">
                                <?= $extra_link['title'] ?>
                            </a>
                        </div>

                    <?php
                    endif; ?>
                </div>
            </div>
        </div>
    </section>

<div class="container">
	<div class="row">
		<div class="col-12">

			<?php
				/**
				 * woocommerce_before_main_content hook.
				 *
				 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
				 * @hooked woocommerce_breadcrumb - 20
				 */
				do_action( 'woocommerce_before_main_content' );
			?>

				<?php while ( have_posts() ) : ?>
					<?php the_post(); ?>

					<?php wc_get_template_part( 'content', 'single-product' ); ?>

				<?php endwhile; // end of the loop. ?>

			<?php
				/**
				 * woocommerce_after_main_content hook.
				 *
				 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
				 */
				do_action( 'woocommerce_after_main_content' );
			?>

			<?php
				/**
				 * woocommerce_sidebar hook.
				 *
				 * @hooked woocommerce_get_sidebar - 10
				 */
				do_action( 'woocommerce_sidebar' );
			?>
		</div>
	</div>
</div>
<?php
get_footer( 'shop' );

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
