<header id="masthead" class="site-header fixed-top">
    <div class="container">
        <a href="#" class="menuBtn">
            <span class="lines"></span>
        </a>
        <button class="menu-button"></button>
        <div class="site-branding">
            <a href="<?php
            echo esc_url(home_url('/')); ?>" rel="home">
                <?php
                $custom_logo_id = get_theme_mod('custom_logo');
                $logo = wp_get_attachment_image_src($custom_logo_id, 'full');
                $logoBlack = get_field('logo_black', 'option');
                if (has_custom_logo()) {
                    echo '<img class="logo-white" src="' . esc_url($logo[0]) . '" alt="' . get_bloginfo('name') . '">';
                    echo '<img class="logo-black" src="' . esc_url($logoBlack['url']) . '" alt="' . get_bloginfo(
                            'name'
                        ) . '">';
                } else {
                    echo '<h1>' . get_bloginfo('name') . '</h1>';
                }
                ?>
            </a>
        </div><!-- .site-branding -->

        <?php
        if (has_nav_menu('menu-main')) : ?>
            <div class="menu-container">

                <div id="site-header-menu" class="site-header-menu">
                    <nav id="site-navigation" class="main-navigation" role="navigation" aria-label="<?php
                    esc_attr_e('Primary Menu', 'icon'); ?>">
                        <?php
                        wp_nav_menu(
                            array(
                                'theme_location' => 'menu-main',
                                'depth' => 3,
                            )
                        );
                        ?>
                    </nav>
                    <div class="header-lang">
                        <?php
                        dynamic_sidebar('lang'); ?>
                    </div>
                    <?php
                    get_template_part('template-parts/widgets/socialmedia'); ?>

                </div>

            </div>
        <?php
        endif; ?>
        <div class="header-cart">
            <a href="<?= wc_get_cart_url(); ?>" class="header-cart__icon">
                <i class="material-icons">shopping_cart</i>
                <div id="mini-cart-count"></div>
            </a>
        </div>

        <div class="account">
            <a href="<?= wc_get_page_permalink( 'myaccount' ); ?>">
                <span class="material-icons">person</span>
            </a>
        </div>
    </div>
</header><!-- #masthead -->

<div class="cart-flyout">
    <div class="cart-flyout--inner">
        <a href="javascript:;" class="btn-close-cart"><i class="far fa-times-circle"></i></a>
        <div class="cart-flyout__content">
            <div class="cart-flyout__heading"><?php esc_html_e('Koszyk', 'icon-concept') ?></div>
            <div class="cart-flyout__loading"></div>
            <div class="widget_shopping_cart_content">
            </div>
        </div>
    </div>
</div>