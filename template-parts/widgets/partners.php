<section class="partners">
    <div class="container">
        <div class="row align-items-center">
            <?php
            $i = 1;
            if (have_rows('partners', 'option')):
                while (have_rows('partners', 'option')): the_row();
                    $image = get_sub_field('logo', 'option');
                    $link = get_sub_field('link', 'option')
                    ?>
                    <div class="col-6 col-md-4 col-lg">
                        <a class="partners__link wow fadeIn" data-wow-duration="1.5s" data-wow-delay="0.<?php echo $i; ?>s" href="<?php echo $link['url'] ?>">
                            <img class="partners__image" src="<?php echo $image['url'] ?>" alt="<?php echo $image['title'] ?>">
                        </a>
                    </div>
                <?php
                $i++;
                endwhile;
            endif; ?>
        </div>
    </div>
</section>