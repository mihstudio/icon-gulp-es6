<?php

$bg = get_field('offer_bg', 'option');
$title = get_field('title_offer', 'option');
$text = get_field('desc_offer', 'option');
$form = get_field('formularz_kontakowy', 'option');
?>

<section class="footer-offer lazy" data-bg="<?= $bg['url'] ?>">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div data-aos="fade-right" data-aos-duration="1000" data-aos-delay="300">
                    <h3 class="section-title topLine"><?php
                        echo $title ?></h3>
                    <div class="subtitle-section"><?php
                        echo $text; ?></div>
                    <div class="icon-contact">
                        <?php
                        if (have_rows('contact_info', 'option')):
                            while (have_rows('contact_info', 'option')) : the_row();
                                $link = get_sub_field('contact_info_text', 'option');
                                $icon = get_sub_field('contact_info_icon', 'option');
                                ?>
                                <a href="<?php
                                echo $link['url'] ?>" class="icon-contact__item">
                                    <img src="<?php
                                    echo $icon['url'] ?>" alt="<?php
                                    echo $icon['title'] ?>">
                                    <?php
                                    echo $link['title'] ?>
                                </a>
                            <?php
                            endwhile;
                        endif;
                        ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div data-aos="fade-left" data-aos-duration="1000" data-aos-delay="400">
                    <?= $form; ?>
                </div>
            </div>
        </div>
    </div>
</section>


