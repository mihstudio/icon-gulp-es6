<div class="social-media">
    <?php
    if (have_rows('social_media', 'option')):
        while (have_rows('social_media', 'option')): the_row();
            $icon = get_sub_field('social_media_icon', 'option');
            $link = get_sub_field('social_media_link', 'option');
            ?>

        <div class="social-media__item">
            <a href="<?php echo $link['url'] ?>">
                <?php echo $icon; ?>
            </a>
        </div>

        <?php
        endwhile;

    endif; ?>
</div>
    