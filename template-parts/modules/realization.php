<?php

$title = get_sub_field('title');
$text = get_sub_field('text');
$btn = get_sub_field('link');
?>

<section class="realization titleCenter">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="titleCenter__wrapper">
                    <h3 class="section-title centerLine" data-aos="zoom-in" data-aos-duration="1000" data-aos-delay="300">
                        <?php
                        echo $title ?>
                    </h3>
                    <div class="section-text" data-aos="zoom-in" data-aos-duration="1000" data-aos-delay="300">
                        <?php
                        echo $text ?>
                    </div>
                    <div class="realization__btn" data-aos="zoom-in" data-aos-duration="1000" data-aos-delay="300">
                        <?php
                        if ($btn): ?>
                            <a href="<?= $btn['url'] ?>" class="btn btn-black"> <?= $btn['title'] ?>
                            </a>
                        <?php
                        endif; ?>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="swiper-container sliderRealizations" data-aos="zoom-in" data-aos-duration="1000" data-aos-delay="300">
                    <div class="swiper-wrapper">
                        <?php
                        $args = array('post_type' => 'realizacje', 'posts_per_page' => 10);
                        $the_query = new WP_Query($args);
                        ?>
                        <?php
                        if ($the_query->have_posts()) :
                            while ($the_query->have_posts()) : $the_query->the_post(); ?>
                                <div class="swiper-slide">
                                    <div class="swiper-slide__bg">
                                        <?php
                                        the_post_thumbnail('medium'); ?>
                                    </div>
                                    <div class="swiper-slide__content">
                                        <div class="swiper-slide__date">
                                            <?php
                                            $post_date = get_field('where_create', $post->ID);
                                            echo $post_date;
                                            ?>
                                        </div>
                                        <h3 class="section-title topLine">
                                            <a href="<?php
                                            the_permalink(); ?>">
                                                <?php
                                                the_title(); ?>
                                            </a>
                                        </h3>
                                        <div class="swiper-slide__text">
                                            <?php
                                            the_excerpt(); ?>
                                        </div>
                                        <div class="swiper-slide__link">
                                            <a href="<?php
                                            the_permalink(); ?>" class="btn btn-black-link">
                                                <?php
                                                _e('Zobacz więcej', 'icon-concept'); ?>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                wp_reset_postdata();
                            endwhile;
                        else: ?>
                            <p><?php
                                _e('Sorry, no posts matched your criteria.', 'icon-concept'); ?></p>
                        <?php
                        endif; ?>

                    </div>
                    <div class="swiper-pagination"></div>
                </div>
            </div>
        </div>
    </div>
</section>

