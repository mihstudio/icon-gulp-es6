<?php

$bg = get_sub_field('bg');
$title = get_sub_field('title');
$text = get_sub_field('text');
$btn = get_sub_field('btn');
?>

<section class="hero lazy" data-bg="<?= $bg['url'] ?>">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12 col-lg-5 col-xl-6">
                <h2 class="section-title" data-aos="fade-right" data-aos-duration="1000" data-aos-delay="300">
                    <?= $title ?>
                </h2>
                <div class="section-text" data-aos="fade-right" data-aos-duration="1000" data-aos-delay="300">
                    <?= $text ?>
                </div>
                <div class="swiper-container sliderHeroHome" data-aos="fade-left" data-aos-duration="1000"
                     data-aos-delay="300">
                    <div class="swiper-wrapper">
                        <?php
                        $i = 1;
                        if (have_rows('slider')):
                            while (have_rows('slider')): the_row();
                                $image = get_sub_field('img');
                                ?>

                                <div class="swiper-slide">
                                    <a data-fancybox="gallery" href="<?= $image['url'] ?>">
                                        <img src='<?php
                                        echo $image['url'] ?>' alt='<?php
                                        echo $image['title'] ?>'>
                                    </a>
                                </div>
                                <?php
                                $i++;
                            endwhile;
                        endif; ?>
                    </div>
                </div>
                <div class="hero__link" data-aos="fade-right" data-aos-duration="1000" data-aos-delay="300">
                    <?php
                    if ($btn): ?>
                        <a href="<?php
                        echo $btn['url'] ?>" class="btn btn-white"> <?php
                            echo $btn['title'] ?>
                        </a>
                    <?php
                    endif; ?>
                </div>
            </div>
            <div class="col-12 col-lg-7 col-xl-6">
                <div class="hero__list" data-aos="fade-left" data-aos-duration="1000" data-aos-delay="300">
                    <?php
                    $i = 1;
                    if (have_rows('slider')):
                        while (have_rows('slider')): the_row();
                            $image = get_sub_field('img');
                            $size = 'medium';
                            $thumb = $image['sizes'][$size];
                            ?>
                            <div class="hero__item hero__slide-<?= $i; ?>">
                                <a data-fancybox="gallery" href="<?= $image['url'] ?>">
                                    <div class="hero__image lazy" data-bg="<?= esc_url($thumb); ?>"></div>
                                </a>
                            </div>
                            <?php
                            $i++;
                        endwhile;
                    endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>

