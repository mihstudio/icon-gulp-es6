<?php

$title = get_sub_field('title');
$text = get_sub_field('text');
$btn = get_sub_field('link');
?>

<section class="models titleCenter">
    <div class="container">
        <div class="row">

            <?php
            $i = 1;
            if (have_rows('model')):
                while (have_rows('model')): the_row();
                    $img = get_sub_field('img');
                    ?>
                    <div class="col-6 col-md-3">
                        <img src="<?php echo $img['url'] ?>" alt="<?php echo $img['title'] ?>" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="<?= $i; ?>00">
                    </div>
                <?php
                $i++;
                endwhile;
            endif; ?>

            <div class="col-12 spacer-10"></div>

            <div class="col-12">
                <div class="titleCenter__wrapper"  data-aos="fade-up" data-aos-duration="1000" data-aos-delay="500">
                    <h3 class="section-title centerLine">
                        <?php
                        echo $title ?>
                    </h3>
                    <div class="section-text">
                        <?php
                        echo $text ?>
                    </div>
                    <div class="hero__btn">
                        <a href="<?php
                        echo $btn['url'] ?>" class="btn btn-black"> <?php
                            echo $btn['title'] ?>
                        </a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

