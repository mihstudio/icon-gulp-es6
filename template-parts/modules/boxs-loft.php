<section class="boxs boxs-loft">
    <div class="container">
        <div class="row">
            <?php
            $i = 1;
            $featured_posts = get_sub_field('choose_pages');
            if ($featured_posts):
                foreach ($featured_posts as $post):
                    setup_postdata($post);
                    $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'medium');
                    $movie_count = count($featured_posts); ?>
                    <div class="col-12 col-md-6 col-xl-<?php if($movie_count > 4) { echo 4; } else { echo (12/$movie_count); } ?> mb-4">
                        <div class="box__item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="<?= $i; ?>00">
                            <div class="box__thumb lazy" data-bg="<?php echo $featured_img_url; ?>"></div>
                            <div class="box__content">
                                <h3 class="box__title"><?php
                                    the_title(); ?></h3>
                                <div class="box__text"><?php
                                    the_excerpt(); ?></div>
                                <div class="box__btn">
                                    <a href="<?php
                                    the_permalink(); ?>" class="btn btn-black">
                                        <?php
                                        _e('Zobacz więcej', 'icon-concept'); ?>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php
                    $i++;
                endforeach;
                wp_reset_postdata();
            endif; ?>

        </div>
    </div>
</section>

