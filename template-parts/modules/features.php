<?php
$bg = get_sub_field('bg');
?>

<section class="features lazy" data-bg="<?php
echo $bg['url'] ?>">
    <div class="container">
        <div class="row">
            <?php
            if (have_rows('elements')):
                while (have_rows('elements')): the_row();
                    $title = get_sub_field('title');
                    $text = get_sub_field('text');
                    ?>
                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="features__box" data-aos="zoom-in" data-aos-duration="1000" data-aos-delay="300">
                            <h3 class="section-title bottomLine">
                                <?php
                                echo $title ?>
                            </h3>
                            <div class="section-text">
                                <?php
                                echo $text ?>
                            </div>
                        </div>
                    </div>
                <?php
                endwhile;
            endif; ?>
        </div>
    </div>
</section>

