<?php

$bg = get_sub_field('image');

if ($bg) : ?>
<section class="icons-text black-overlay lazy" data-bg="<?php
echo $bg['url'] ?>">
    <?php
    else : ?>
    <section class="icons-text black-text lazy">
        <?php
        endif;
        ?>
        <div class="container">
            <div class="row">
                <?php
                if (have_rows('icon')):
                    while (have_rows('icon')): the_row();
                        $image = get_sub_field('image');
                        $title = get_sub_field('title');
                        $text = get_sub_field('text');
                        ?>
                        <div class="col-12 col-md-6 ">
                            <div class="features__box" data-aos="zoom-in" data-aos-duration="1000" data-aos-delay="300">
                                <div class="features__image">
                                    <img src="<?php
                                    echo $image['url'] ?>" alt="<?php
                                    echo $image['title'] ?>">
                                </div>
                                <div class="features__content">
                                    <h3 class="section-title topLine">
                                        <?php
                                        echo $title ?>
                                    </h3>
                                    <div class="section-text">
                                        <?php
                                        echo $text ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php
                    endwhile;
                endif; ?>
            </div>
        </div>
    </section>

