<?php
$bg = get_sub_field('bg');
$img = get_sub_field('logo');
$text = get_sub_field('text');
$btn = get_sub_field('link');
?>

<section class="section-about lazy" data-bg="<?php echo $bg['url'] ?>">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-4">
                <div class="section-about__pic" data-aos="fade-right" data-aos-duration="1000" data-aos-delay="300">
                    <img src='<?php echo $img['url'] ?>' alt="" class="img-fluid">
                </div>
            </div>
            <div class="col-12 col-lg-8">
                <div class="section-text" data-aos="fade-left" data-aos-duration="1000" data-aos-delay="300">
                    <?php echo $text ?>
                </div>
                <div class="section-about__btn" data-aos="fade-left" data-aos-duration="1000" data-aos-delay="300">
                    <a href="<?php echo $btn['url'] ?>" class="btn btn-white"> <?php
                        echo $btn['title'] ?>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

