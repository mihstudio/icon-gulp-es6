<?php
$title = get_sub_field('title');
$text = get_sub_field('text');
$btn = get_sub_field('link');
?>

<section class="realization-gallery titleCenter">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="titleCenter__wrapper" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="200">
                    <h3 class="section-title centerLine">
                        <?php
                        echo $title ?>
                    </h3>
                    <div class="section-text">
                        <?php
                        echo $text ?>
                    </div>
                    <div class="hero__btn">
                        <?php if($btn): ?>
                        <a href="<?= $btn['url'] ?>" class="btn btn-black"> <?= $btn['title'] ?> </a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="realization-gallery__list">
                    <?php
                    $i = 1;
                    if (have_rows('gallery')):
                        while (have_rows('gallery')): the_row();
                            $image = get_sub_field('image');
                            ?>
                            <div class="realization-gallery__item hero__slide-<?php echo $i; ?>" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="<?= $i ?>00">
                                <a data-fancybox="gallery" href="<?php
                                echo $image['url'] ?>">
                                    <div class="realization-gallery__image lazy" data-bg="<?php echo $image['url'] ?>"> </div>
                                </a>
                            </div>
                            <?php
                            $i++;
                        endwhile;
                    endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>

