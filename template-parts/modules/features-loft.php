<?php
$bg = get_sub_field('image');
?>

<section class="features black-overlay lazy" data-bg="<?php
echo $bg['url'] ?>">
    <div class="container">
        <div class="row">
            <?php
            $i = 1;
            if (have_rows('feature')):
                while (have_rows('feature')): the_row();
                    $title = get_sub_field('title');
                    $text = get_sub_field('text');
                    $btn = get_sub_field('link');
                    ?>
                    <div class="col-12 col-lg-6">
                        <div class="features__box centerBox" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="<?= $i; ?>00">
                            <h3 class="section-title centerLine">
                                <?php
                                echo $title ?>
                            </h3>
                            <div class="section-text">
                                <?php
                                echo $text ?>
                            </div>
                            <div class="features__btn">
                                <a href="<?php echo $btn['url'] ?>" class="btn btn-white"> <?php
                                    echo $btn['title'] ?>
                                </a>
                            </div>
                        </div>
                    </div>
                <?php
                $i++;
                endwhile;
            endif; ?>
        </div>
    </div>
</section>

