<?php

$bg = get_sub_field('image');
$title = get_sub_field('title');
$btn = get_sub_field('link');
?>

<section class="hero hero-page lazy" data-bg="<?php echo $bg['url'] ?>">
    <div class="container">
        <div class="row align-items-end">
            <div class="col-12">
                <h2 class="section-title">
                    <?php
                    echo $title ?>
                </h2>
                <?php
                if ($btn) : ?>
                    <div class="hero__link">
                        <a href="<?php
                        echo $btn['url'] ?>" class="btn btn-white"> <?php
                            echo $btn['title'] ?>
                        </a>
                    </div>
                <?php
                endif; ?>
            </div>
        </div>
    </div>
</section>

