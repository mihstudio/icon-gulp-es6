<?php

$choose = get_sub_field('choose_img');
$img = get_sub_field('image');
$title = get_sub_field('title');
$text = get_sub_field('text');
?>

<section class="img-text-box <?php
echo $choose; ?>">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12 col-lg-6 <?php
            if ($choose === 'right') {
                echo 'order-1 order-lg-2';
            } ?>">
                <div class="img-text-box__photo" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="300">
                    <img class="img-text-box__image lazy" src="<?= $img['url']; ?>">
                </div>
                <?php
                $link_box = get_sub_field('link'); ?>
                <?php
                if ($link_box) : ?>
                    <div class="img-text-box__link">
                        <a class="btn btn-yellow" href="<?php
                        echo $link_box['url']; ?>"><?php
                            echo $link_box['title']; ?></a>
                    </div>
                <?php
                endif; ?>
            </div>
            <div class="col-12 col-lg-6 <?php
            if ($choose === 'right') {
                echo 'order-2 order-lg-1 pl-auto pl-lg-2';
            } else {
                echo 'pl-auto pl-lg-5';
            } ?>">
                <div data-aos="fade-down" data-aos-duration="1000" data-aos-delay="400">
                    <?php
                    if ($title): ?>
                        <h3 class="section-title topLine">
                            <?= $title ?>
                        </h3>
                    <?php
                    endif; ?>
                    <div class="img-text-box__content">
                        <?= $text; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
