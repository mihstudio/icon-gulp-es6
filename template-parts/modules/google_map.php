

<section class="google-map">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <?php
                $location = get_sub_field('map');
                if( $location ): ?>
                    <div class="acf-map" data-zoom="16">
                        <div class="marker" data-lat="<?php echo esc_attr($location['lat']); ?>" data-lng="<?php echo esc_attr($location['lng']); ?>"></div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
