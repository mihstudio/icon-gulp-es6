<?php
$col = get_sub_field('choose_col'); ?>

<section class="column-text">
    <div class="container">
        <div class="row">
            <?php
            if (have_rows('block_text')) :
            while (have_rows('block_text')) :
            the_row();
            $title = get_sub_field('title');
            $text = get_sub_field('text');
            $link = get_sub_field('link'); ?>


            <?php
            if ($col === 'col2'): ?>
            <div class="col-12 col-md-6">
                <?php
                elseif ($col === 'col3'): ?>
                <div class="col-12 col-md-4">
                    <?php
                    elseif ($col === 'col4'): ?>
                    <div class="col-12 col-md-6 col-lg-3">
                        <?php
                        endif; ?>
                        <div data-aos="fade-down" data-aos-duration="1000" data-aos-delay="400">
                            <?php
                            if ($title): ?>
                                <h3 class="section-title topLine">
                                    <?= $title ?>
                                </h3>
                            <?php
                            endif; ?>
                            <div class="img-text-box__content">
                                <?= $text; ?>
                            </div>
                            <?php
                            if ($link) : ?>
                                <a class="btn btn-black" href="<?= esc_url($link['url']); ?>"
                                   target="<?= esc_attr($link['target']); ?>">
                                    <?= esc_html($link['title']); ?>
                                </a>
                            <?php
                            endif; ?>
                        </div>
                    </div>

                    <?php
                    endwhile;
                    else :
                        // no rows found
                    endif; ?>
                </div>
            </div>
</section>


