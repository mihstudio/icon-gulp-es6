<?php
$title = get_sub_field('title');
$text = get_sub_field('text');
$btn = get_sub_field('link');
?>

<section class="text-center">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <div class="titleCenter__wrapper">
                    <h3 class="section-title centerLine" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="300">
                        <?php
                        echo $title ?>
                    </h3>
                    <div class="section-text" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="400">
                        <?php
                        echo $text ?>
                    </div>
                    <div class="hero__btn" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="500">
                        <?php if($btn): ?>
                            <a href="<?php echo $btn['url'] ?>" class="btn btn-black"> <?php
                                echo $btn['title'] ?>
                            </a>
                        <?php endif; ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>