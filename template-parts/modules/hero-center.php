<?php

global $post;
global $wp_query;

$bg = get_sub_field('image');
$title = get_sub_field('title');
$btn = get_sub_field('link');
$parent = get_the_title($post->post_parent);
?>

<section class="hero hero-center lazy" data-bg="<?php
echo $bg['url'] ?>">
    <div class="container">
        <div class="row align-items-end">
            <div class="col-12 text-center">
                <h3 class="parent-title"  data-aos="fade-up" data-aos-duration="1000" data-aos-delay="300">
                    <?php
                    if ($post->post_parent != 0): ?>
                        <?= $parent ?> >
                    <?php
                    endif; ?>
                </h3>
                <h2 class="section-title text-center" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="400">
                    <?php
                    if ($title):
                        echo $title;
                    else:
                        the_title();
                    endif;
                    ?>
                </h2>
            </div>
        </div>
    </div>
</section>

