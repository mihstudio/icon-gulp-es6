<?php
$image = get_sub_field('image');
$title = get_sub_field('title');
$text = get_sub_field('text');
$btn = get_sub_field('link');
?>

<section class="partner-section">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-6">
                <img src="<?php
                echo $image['url'] ?>" alt="<?php
                echo $image['title'] ?>" data-aos="fade-right" data-aos-duration="1000" data-aos-delay="300">
            </div>

            <div class="col-12 col-lg-6">
                <div data-aos="fade-left" data-aos-duration="1000" data-aos-delay="400">
                    <h3 class="section-title topLine ml-lg-2">
                        <?php
                        echo $title ?>
                    </h3>
                    <div class="section-text ml-lg-2">
                        <?php
                        echo $text ?>
                    </div>
                    <div class="movie__btn ml-lg-2">
                        <a href="<?php
                        echo $btn['url'] ?>" class="btn btn-black"> <?php
                            echo $btn['title'] ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

