<?php

$title_products_section = get_sub_field('title_section');
$get_category = get_sub_field('get_category'); ?>


<section class="products-widget">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h3 class="section-title centerLine" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="300">
                    <?= $title_products_section; ?>
                </h3>
            </div>
            <?php
            $get_category = get_sub_field('get_category');
            $term = get_term_by('id', $get_category, 'product_cat');

            $params = array(
                'posts_per_page' => 4,
                'post_type' => 'product',
                'meta_value' => '0',
                'meta_compare' => '>=',
                'tax_query' => array(
                    'relation' => 'AND',
                    array(
                        'taxonomy' => 'product_cat',
                        'field' => 'slug',
                        'terms' => $term->name
                    )
                ),
            );
            $wc_query = new WP_Query($params);


            $i = 1;
            if ($wc_query->have_posts()) :
                while ($wc_query->have_posts()) :
                    $wc_query->the_post();
                    $price = get_post_meta(get_the_ID(), '_price', true);
                    ?>
                    <div class="col-12 col-md-6 col-lg-3">
                        <div class="small-product" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="<?= $i ?>00">
                            <a href="<?php
                            the_permalink(); ?>">
                                <div class="small-product__photo">
                                    <?= get_the_post_thumbnail($wc_query->ID, 'thumbnail'); ?>
                                </div>
                                <h3 class="section-title bottomLine">
                                    <?php
                                    the_title(); ?>
                                </h3>
                            </a>
                            <div class="small-product__price">
                                <?= wc_price($price); ?>
                            </div>
                            <a href="<?php
                            the_permalink(); ?>" class="btn btn-black">
                                <?php
                                _e('Sprawdź produkt', 'icon-concept'); ?>
                            </a>
                        </div>
                    </div>
                <?php
                $i++;
                endwhile;
                wp_reset_postdata();
            else:
                _e('Brak produktów');
            endif; ?>

        </div>
    </div>
</section>
