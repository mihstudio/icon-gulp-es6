<?php
$movie_img = get_sub_field('movie_img');
$movie = get_sub_field('movie_url');
$title = get_sub_field('title');
$text = get_sub_field('text');
$btn = get_sub_field('link');
?>

<section class="movie" >
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12 col-lg-6">
                <div class="movie__wrapper" data-aos="fade-left" data-aos-duration="1000" data-aos-delay="300">
                    <div id="player" data-plyr-provider="youtube" data-poster="<?php echo $movie_img['url']; ?>" data-plyr-embed-id="<?php echo $movie ?>"></div>
                </div>
            </div>

            <div class="col-12 col-lg-6">
                <h2 class="section-title topLine ml-lg-2" data-aos="fade-right" data-aos-duration="1000" data-aos-delay="300">
                    <?php
                    echo $title ?>
                </h2>
                <div class="section-text ml-lg-2" data-aos="fade-right" data-aos-duration="1000" data-aos-delay="300">
                    <?php echo $text ?>
                </div>
                <div class="movie__btn ml-0 ml-lg-2" data-aos="fade-right" data-aos-duration="1000" data-aos-delay="300">
                    <a href="<?php echo $btn['url'] ?>" class="btn btn-black"> <?php
                        echo $btn['title'] ?>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

