<?php

$gallery_images = get_sub_field('gallery');
$link = get_sub_field('link');
$text = get_sub_field('text'); ?>

<section class="gallery-with-text">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-6">
                <div class="gallery-big">
                <?php
                $i=1;
                if ($gallery_images) :
                    foreach ($gallery_images as $gallery_image): ?>
                        <a data-fancybox="gallery" href="<?php
                        echo esc_url($gallery_image['url']); ?>"  data-aos="fade-up" data-aos-duration="1000" data-aos-delay="<?= $i ?>00">
                            <img src="<?php
                            echo esc_url($gallery_image['sizes']['medium']); ?>" alt="<?php
                            echo esc_attr($gallery_image['alt']); ?>"/>
                        </a>
                    <?php
                    $i++;
                    endforeach;
                endif; ?>
                </div>
            </div>
            <div class="col-12 col-lg-6">
                <div class="img-text-box__content" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="300">
                    <?= $text; ?>
                </div>

                <?php
                if ($link) : ?>
                <div class="gallery-big__btn">
                    <a class="btn btn-black" href="<?php
                    echo esc_url($link['url']); ?>" target="<?php
                    echo esc_attr($link['target']); ?>" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="300">
                        <?php
                        echo esc_html($link['title']); ?>
                    </a>
                </div>
                <?php
                endif; ?>
            </div>
        </div>
    </div>
</section>
