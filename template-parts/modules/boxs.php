<section class="boxs">
    <div class="container">
        <div class="row">

            <?php
            if (have_rows('category_box')):
                while (have_rows('category_box')): the_row();
                    $title = get_sub_field('title');
                    $text = get_sub_field('text');
                    $btn = get_sub_field('link');
                    $image = get_sub_field('image');
                    ?>
                    <div class="col-12 col-md-6 col-lg-4 mb-4">
                        <div class="box-products lazy" data-bg="<?php echo $image['url'] ?>">
                            <h3 class="section-title">
                                <?php
                                echo $title ?>
                            </h3>
                            <div class="section-text">
                                <?php
                                echo $text ?>
                            </div>
                                <?php
                                $featured_posts = get_sub_field('pages');
                                if ($featured_posts): ?>
                                    <ul class="box-products__list">
                                        <?php foreach ($featured_posts as $post):
                                            setup_postdata($post); ?>
                                            <li>
                                                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                            </li>
                                        <?php
                                        endforeach; ?>
                                    </ul>
                                    <?php
                                    wp_reset_postdata(); ?>
                                <?php
                                endif; ?>
                            <div class="hero__btn">
                                <a href="<?php echo $btn['url'] ?>" class="btn btn-black"> <?php echo $btn['title'] ?>
                                </a>
                            </div>
                        </div>
                    </div>

                <?php
                endwhile;
            endif; ?>

        </div>
    </div>
</section>

