<?php

if( have_rows('sections') ):
    while ( have_rows('sections') ) : the_row();

        if( get_row_layout() === 'hero' ):
            get_template_part('template-parts/modules/hero-center');

        elseif( get_row_layout() === 'boxs' ):
            get_template_part('template-parts/modules/boxs-loft');

        elseif( get_row_layout() === 'movie' ):
            get_template_part('template-parts/modules/movie');

        elseif( get_row_layout() === 'features' ):
            get_template_part('template-parts/modules/features-loft');

        elseif( get_row_layout() === 'models' ):
            get_template_part('template-parts/modules/models');

        elseif( get_row_layout() === 'realization' ):
            get_template_part('template-parts/modules/realization-gallery');

        elseif( get_row_layout() === 'icons' ):
            get_template_part('template-parts/modules/icons-with-text');

        elseif( get_row_layout() === 'partner' ):
            get_template_part('template-parts/modules/image-left');

        elseif ( get_row_layout() === 'show_offer' ):
            $switch = get_sub_field('switch_offer');

            if ($switch === true) {
                get_template_part('template-parts/widgets/section-offer');
            };

        elseif( get_row_layout() === 'img_text' ):
            get_template_part('template-parts/modules/image-with-text');

        elseif ( get_row_layout() === 'column_text' ) :
            get_template_part('template-parts/modules/column_text');

        elseif( get_row_layout() === 'text_center' ):
            get_template_part('template-parts/modules/text-center');

        elseif ( get_row_layout() === 'show_products_cat' ) :
            get_template_part('template-parts/modules/show_products_cat');

        elseif ( get_row_layout() === 'gallery_with_text' ) :
            get_template_part('template-parts/modules/gallery_with_text');

        elseif ( get_row_layout() === 'google_map' ) :
            get_template_part('template-parts/modules/google_map');

        endif;
    endwhile;

else :
endif;



?>