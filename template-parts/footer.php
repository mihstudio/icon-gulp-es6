<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 col-lg-4">
                <div class="footerBox">
                    <div class="footerBox__logo">
                        <?php
                        $custom_logo_id = get_theme_mod('custom_logo');
                        $logo = wp_get_attachment_image_src($custom_logo_id, 'full');
                        echo '<img class="logo-white" src="' . esc_url($logo[0]) . '" alt="' . get_bloginfo(
                                'name'
                            ) . '">'; ?>
                    </div>
                    <div class="footerBox__text">
                        <?php
                        $footer_text = get_field('info_footer', 'option');
                        echo $footer_text; ?>
                    </div>
                    <?php
                    get_template_part('template-parts/widgets/socialmedia'); ?>
                    <div class="footerBox__text krs">
                        <?php
                        $footer_textKrs = get_field('info_krs_footer', 'option');
                        echo $footer_textKrs; ?>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-4">
                <div class="footerBox">
                    <h3 class="footerBox__title"><?php
                        _e('Produkty', 'icon-concept'); ?></h3>
                    <?php
                    wp_nav_menu(array('theme_location' => 'menu-footer', 'menu_id' => 'footerMenu__products')); ?>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-2">
                <div class="footerBox">
                    <h3 class="footerBox__title"><?php
                        _e('Wiedza', 'icon-concept'); ?></h3>
                    <?php
                    wp_nav_menu(array('theme_location' => 'menu-footer-2', 'menu_id' => 'footerMenu__know')); ?>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-2">
                <div class="footerBox">
                    <h3 class="footerBox__title"><?php
                        _e('Dokumenty', 'icon-concept'); ?></h3>
                    <?php
                    wp_nav_menu(array('theme_location' => 'menu-footer-3', 'menu_id' => 'footerMenu__doc')); ?>
                </div>
            </div>
        </div>
    </div>
</footer>