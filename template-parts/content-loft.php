<?php

if( have_rows('sections') ):
    while ( have_rows('sections') ) : the_row();

        // Case: Paragraph layout.
        if( get_row_layout() === 'hero' ):
            get_template_part('template-parts/modules/hero-page');

        elseif( get_row_layout() === 'boxs' ):
            get_template_part('template-parts/modules/boxs-loft');

        elseif( get_row_layout() === 'movie' ):
            get_template_part('template-parts/modules/movie');

        elseif( get_row_layout() === 'features' ):
            get_template_part('template-parts/modules/features-loft');

        elseif( get_row_layout() === 'models' ):
            get_template_part('template-parts/modules/models');

        elseif( get_row_layout() === 'realization' ):
            get_template_part('template-parts/modules/realization-gallery');

        elseif( get_row_layout() === 'icons' ):
            get_template_part('template-parts/modules/icons-with-text');

        elseif( get_row_layout() === 'partner' ):
            get_template_part('template-parts/modules/partner-section');


        endif;
    endwhile;

else :
endif;

?>