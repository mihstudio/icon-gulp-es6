import $ from "jquery";
import Swiper, {Navigation, Pagination} from 'swiper';

Swiper.use([Navigation, Pagination]);


const swiper = new Swiper('.sliderRealizations', {
    slidesPerView: 1.3,
    spaceBetween: 20,
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
    breakpoints: {
        1366: {
            slidesPerView: 4,
            spaceBetween: 30,
        },
        1024: {
            slidesPerView: 3,
            spaceBetween: 20,
        },
        768: {
            slidesPerView: 2,
            spaceBetween: 20,
        },
        500: {
            slidesPerView: 1.3,
            spaceBetween: 20,
        },
    },
});

const swiperBlog = new Swiper('.sliderBlog', {
    slidesPerView: 1.3,
    spaceBetween: 20,
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
    breakpoints: {
        1366: {
            slidesPerView: 3,
            spaceBetween: 30,
        },
        1024: {
            slidesPerView: 3,
            spaceBetween: 20,
        },
        768: {
            slidesPerView: 2,
            spaceBetween: 20,
        },
        500: {
            slidesPerView: 1.5,
            spaceBetween: 20,
        },
    },
});

const swiperHeroHome = new Swiper('.sliderHeroHome', {
    slidesPerView: 2.5,
    spaceBetween: 10,
});