import 'babel-polyfill';
import $ from "jquery";
import './base';
import './header';
import './carousels';
import './fancyboxInit';
import './playerInit';
import './add_to_cart';
import './menu';
import './map';
import AOS from 'aos';

//const WOW = require('wowjs');
AOS.init();

// window.wow = new WOW.WOW({
//     live: false
// });
//
// window.wow.init();

$('.show-filters').on('click', function () {
    $('.shop-filters').toggleClass('d-none');
    $('.shop-filters').trigger('slideDown');
});

