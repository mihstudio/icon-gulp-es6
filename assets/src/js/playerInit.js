import Plyr from "plyr";


const player = new Plyr('#player', {
    controls: ['play-large', 'play', 'progress', 'current-time', 'mute', 'volume',],
    youtube: {noCookie: false, rel: 1, showinfo: 1, iv_load_policy: 3, modestbranding: 0 },
});

