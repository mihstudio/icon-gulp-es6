import $ from "jquery";

$(window).scroll(function() {
    const scroll = $(window).scrollTop();

    if (scroll >= 100) {
        $(".site-header").addClass("darkHeader");
    } else {
        $(".site-header").removeClass("darkHeader");
    }
});



// $(document).ready(function(){
//     // menu click event
//     $('.menuBtn').click(function() {
//         $(this).toggleClass('act');
//         $('body').toggleClass('no-scroll');
//         $(".site-header").toggleClass("blackBg");
//         if($(this).hasClass('act')) {
//             $('.main-navigation').addClass('act');
//         }
//         else {
//             $('.main-navigation').removeClass('act');
//         }
//     });
//
//
// });