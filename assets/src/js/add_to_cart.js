// jQuery(document).ready(function ($) {
//
//     $('.single_add_to_cart_button').on('click', function (e) {
//         e.preventDefault();
//
//         var $thisbutton = $(this),
//             $form = $thisbutton.closest('form.cart'),
//             id = $thisbutton.val(),
//             product_qty = $form.find('input[name=quantity]').val() || 1,
//             product_id = $form.find('input[name=product_id]').val() && id,
//             variation_id = $form.find('input[name=variation_id]').val() || 0;
//
//         var data = {
//             action: 'ql_woocommerce_ajax_add_to_cart',
//             product_id: product_id,
//             product_sku: '',
//             quantity: product_qty,
//             variation_id: variation_id,
//         };
//
//         $.ajax({
//             type: 'post',
//             url: wc_add_to_cart_params.ajax_url,
//             data: data,
//             beforeSend: function (response) {
//                 $thisbutton.removeClass('added').addClass('loading');
//             },
//
//             complete: function (response) {
//                 $thisbutton.addClass('added').removeClass('loading');
//
//             },
//
//             success: function (response) {
//                 if (response.error & response.product_url) {
//                     window.location = response.product_url;
//                     return;
//                 } else {
//                     $(document.body).trigger('added_to_cart', [response.fragments, response.cart_hash, $thisbutton]);
//                 }
//             },
//         });
//     });
// });
const $window = $(window),
    $document = $(document),
    $htmlbody = $('html,body'),
    $body = $(document.body);
$(document.body).trigger('wc_fragment_refresh');


$document.on('click', '.header-cart', function(e){
    if($window.width() > 767){
        e.preventDefault();
        $body.toggleClass('open-cart-aside');
    }
});

$document.on('click', '.btn-close-cart', function(e){
    e.preventDefault();
    $body.removeClass('open-cart-aside');
});

$document.on( 'adding_to_cart', function( e ){
    $body.addClass('open-cart-aside');
    $('.cart-flyout').addClass('cart-flyout--loading');
    $('.header-toggle-cart > a > i,.la_com_action--cart > a > i').removeClass('negan-icon-bag').addClass('fa-spinner fa-spin');
});