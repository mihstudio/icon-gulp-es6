import $ from "jquery";
import "@fancyapps/fancybox";

$('[data-fancybox="gallery"]').fancybox({
    thumbs : {
        autoStart : true,
        axis: "y"
    },
});