<?php


// Register Custom Post Type Realizacje
function create_realizacje_cpt() {

    $labels = array(
        'name' => _x( 'realizacje', 'Post Type General Name', 'icon-concept' ),
        'singular_name' => _x( 'Realizacje', 'Post Type Singular Name', 'icon-concept' ),
        'menu_name' => _x( 'Realizacje', 'Admin Menu text', 'icon-concept' ),
        'name_admin_bar' => _x( 'Realizacje', 'Add New on Toolbar', 'icon-concept' ),
        'archives' => __( 'Realizacje Archives', 'icon-concept' ),
        'attributes' => __( 'Realizacje Attributes', 'icon-concept' ),
        'parent_item_colon' => __( 'Parent Realizacje:', 'icon-concept' ),
        'all_items' => __( 'All realizacje', 'icon-concept' ),
        'add_new_item' => __( 'Add New Realizacje', 'icon-concept' ),
        'add_new' => __( 'Add New', 'icon-concept' ),
        'new_item' => __( 'New Realizacje', 'icon-concept' ),
        'edit_item' => __( 'Edit Realizacje', 'icon-concept' ),
        'update_item' => __( 'Update Realizacje', 'icon-concept' ),
        'view_item' => __( 'View Realizacje', 'icon-concept' ),
        'view_items' => __( 'View realizacje', 'icon-concept' ),
        'search_items' => __( 'Search Realizacje', 'icon-concept' ),
        'not_found' => __( 'Not found', 'icon-concept' ),
        'not_found_in_trash' => __( 'Not found in Trash', 'icon-concept' ),
        'featured_image' => __( 'Featured Image', 'icon-concept' ),
        'set_featured_image' => __( 'Set featured image', 'icon-concept' ),
        'remove_featured_image' => __( 'Remove featured image', 'icon-concept' ),
        'use_featured_image' => __( 'Use as featured image', 'icon-concept' ),
        'insert_into_item' => __( 'Insert into Realizacje', 'icon-concept' ),
        'uploaded_to_this_item' => __( 'Uploaded to this Realizacje', 'icon-concept' ),
        'items_list' => __( 'realizacje list', 'icon-concept' ),
        'items_list_navigation' => __( 'realizacje list navigation', 'icon-concept' ),
        'filter_items_list' => __( 'Filter realizacje list', 'icon-concept' ),
    );
    $args = array(
        'label' => __( 'Realizacje', 'icon-concept' ),
        'description' => __( '', 'icon-concept' ),
        'labels' => $labels,
        'menu_icon' => 'dashicons-admin-customizer',
        'supports' => array('title', 'editor', 'excerpt', 'thumbnail', 'comments', 'page-attributes', 'custom-fields'),
        'taxonomies' => array(),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => true,
        'hierarchical' => false,
        'exclude_from_search' => false,
        'show_in_rest' => true,
        'query_var' => 'realizacje',
        'publicly_queryable' => true,
        'capability_type' => 'post',
    );
    register_post_type( 'realizacje', $args );

}
add_action( 'init', 'create_realizacje_cpt', 0 );




