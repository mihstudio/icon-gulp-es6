<?php

// MENUS
function _custom_theme_register_menu()
{
    register_nav_menus(
        array(
            'menu-main' => __( 'Menu' ),
            'menu-footer' => __( 'Menu footer produkty' ),
            'menu-footer-2' => __( 'Menu footer wiedza' ),
            'menu-footer-3' => __( 'Menu footer dokumenty' ),
        )
    );
}
add_action( 'init', '_custom_theme_register_menu' );

function custom_setup() {
	// IMAGES
	add_theme_support( 'post-thumbnails' );

	// TITLE TAGS
	add_theme_support('title-tag');
    add_post_type_support( 'page', 'excerpt' );

	// LANGUAGES
	load_theme_textdomain('icon-concept', get_template_directory() . '/languages');

	// HTML 5 - Example : deletes type="*" in scripts and style tags
	add_theme_support( 'html5', [ 'script', 'style' ] );

    add_filter( 'use_block_editor_for_post', '__return_false' );

    add_filter( 'use_block_editor_for_post_type', function( $enabled, $post_type ) {
        return 'your_post_type' === $post_type ? false : $enabled;
    }, 10, 2 );


}
add_action('after_setup_theme', 'custom_setup');

// Move Yoast to bottom
function yoasttobottom() {
	return 'low';
}
add_filter( 'wpseo_metabox_prio', 'yoasttobottom');

// Remove WP Emoji
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

add_theme_support( 'custom-logo' );

// delete wp-embed.js from footer
function my_deregister_scripts(){
	wp_deregister_script( 'wp-embed' );
}
add_action( 'wp_footer', 'my_deregister_scripts' );

// delete jquery migrate
function dequeue_jquery_migrate( &$scripts){
	if(!is_admin()){
		$scripts->remove( 'jquery');
		$scripts->add( 'jquery', false, array( 'jquery-core' ), '1.12.4', true );
	}
}
add_filter( 'wp_default_scripts', 'dequeue_jquery_migrate' );

// force all scripts to load in footer
function clean_header() {
	remove_action('wp_head', 'wp_print_scripts');
	remove_action('wp_head', 'wp_print_head_scripts', 9);
	remove_action('wp_head', 'wp_enqueue_scripts', 1);
}
add_action('wp_enqueue_scripts', 'clean_header');

// add SVG to allowed file uploads
function add_file_types_to_uploads($mime_types){
	$mime_types['svg'] = 'image/svg+xml';

	return $mime_types;
}
add_action('upload_mimes', 'add_file_types_to_uploads', 1, 1);

// disabling big image sizes scaled
add_filter( 'big_image_size_threshold', '__return_false' );

function custom_menu_page_removing()
{
    remove_menu_page('edit-comments.php');
}
add_action('admin_menu', 'custom_menu_page_removing');


remove_action('admin_print_scripts', 'print_emoji_detection_script');
remove_action('admin_print_styles', 'print_emoji_styles');
remove_action('wp_print_styles', 'print_emoji_styles');
remove_action('wp_head', 'wp_oembed_add_discovery_links');
remove_action('wp_head', 'rest_output_link_wp_head');
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'wp_shortlink_wp_head');
remove_action('wp_head', 'rel_canonical');


add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}

//function my_acf_init() {
//    acf_update_setting('google_api_key', 'AIzaSyC7lC738CtpQbqbJMsqOEt0FwSAyUK5UWk');
//}
//add_action('acf/init', 'my_acf_init');