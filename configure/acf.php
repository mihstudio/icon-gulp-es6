<?php

// ACF functions here
if (function_exists('acf_add_options_page')) {
    acf_add_options_page(
        array(
            'page_title' => 'Theme General Settings',
            'menu_title' => 'Theme Settings',
            'menu_slug' => 'theme-general-settings',
            'capability' => 'edit_posts',
            'redirect' => false
        )
    );

    acf_add_options_sub_page(
        array(
            'page_title' => 'Theme Header Settings',
            'menu_title' => 'Header',
            'parent_slug' => 'theme-general-settings',
        )
    );

    acf_add_options_sub_page(
        array(
            'page_title' => 'Theme Footer Settings',
            'menu_title' => 'Footer',
            'parent_slug' => 'theme-general-settings',
        )
    );
}


function my_acf_google_map_api($api)
{
    $api['key'] = 'AIzaSyAOLpp0cD8qx6JgujRX736MzYENnTlvGMU';
    return $api;
}
add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');

function my_theme_add_scripts()
{
    ?>
    <script async type='text/javascript' src='https://maps.googleapis.com/maps/api/js?key=AIzaSyAOLpp0cD8qx6JgujRX736MzYENnTlvGMU'></script>
    <?php
}
add_action('wp_enqueue_scripts', 'my_theme_add_scripts');


