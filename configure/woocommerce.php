<?php
/**
 * Woocommerce only code
 *
 * @package starter
 */

defined('ABSPATH') || exit;

/**
 * Add woocommerce support
 *
 * @since starter 1.0
 */
function starter_add_woocommerce_support()
{
    add_theme_support('woocommerce');
}

add_action('after_setup_theme', 'starter_add_woocommerce_support');

/**
 * Extend comment feature
 */
require_once get_stylesheet_directory() . '/configure/woo/comment/comment.php';

/**
 * Extend filter feature
 */
require_once get_stylesheet_directory() . '/configure/woo/filter/filter.php';

/**
 * Extend upsell/related feature
 */
require_once get_stylesheet_directory() . '/configure/woo/upsell-related.php';
require_once get_stylesheet_directory() . '/configure/woo/add-to-cart.php';

/**
 * Remove woo assets
 *
 * @since starter 1.0
 */

/**
 * Fix search: remove aws plugin's parameter
 *
 * @param string $markup .
 * @since starter 1.0
 *
 */
function starter_aws_searchbox_markup($markup)
{
    $pattern = '/(<input type="hidden" name="type_aws" value="true">)/i';
    $markup = preg_replace($pattern, '', $markup);
    return $markup;
}

add_filter('aws_searchbox_markup', 'starter_aws_searchbox_markup');

/**
 * Show cart contents / total Ajax
 */
function starter_woocommerce_header_add_to_cart_fragment($fragments)
{
    global $woocommerce;
    ob_start();
    ?>
    <span class="notifications_text"><?php
        echo $woocommerce->cart->cart_contents_count; ?></span>
    <?php
    $fragments['.cart-counter .notifications_text'] = ob_get_clean();
    return $fragments;
}

add_filter('woocommerce_add_to_cart_fragments', 'starter_woocommerce_header_add_to_cart_fragment');
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5);
add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_rating', 5);


add_theme_support('wc-product-gallery-lightbox');

function icon_add_woocommerce_support()
{
    add_theme_support(
        'woocommerce',
        array(
            'thumbnail_image_width' => 300,
            'single_image_width' => 300,

            'product_grid' => array(
                'default_rows' => 3,
                'min_rows' => 2,
                'max_rows' => 8,
                'default_columns' => 3,
                'min_columns' => 2,
                'max_columns' => 3,
            ),
        )
    );
}

add_action('after_setup_theme', 'icon_add_woocommerce_support');

function woo_related_products_limit()
{
    global $product;

    $args['posts_per_page'] = 4;
    return $args;
}

add_filter('woocommerce_output_related_products_args', 'jk_related_products_args', 20);
function jk_related_products_args($args)
{
    $args['posts_per_page'] = 4; // 4 related products
    $args['columns'] = 4; // arranged in 2 columns
    return $args;
}

add_filter('woocommerce_add_to_cart_fragments', 'wc_refresh_mini_cart_count');
function wc_refresh_mini_cart_count($fragments)
{
    ob_start();
    ?>
    <div id="mini-cart-count">
        <?php
        echo WC()->cart->get_cart_contents_count(); ?>
    </div>
    <?php
    $fragments['#mini-cart-count'] = ob_get_clean();
    return $fragments;
}