<?php

get_header();
global $post;
global $wp_query;

$custom_hero_bg = get_field('custom_hero_bg');
$dif_title = get_field('different_title');
$extra_link = get_field('extra_link');
$parent = get_the_title($post->post_parent);
?>

    <section class="hero hero-center lazy" data-bg="<?php
    echo $custom_hero_bg['url'] ?>">
        <div class="container">
            <div class="row align-items-end">
                <div class="col-12 text-center">
                    <h3 class="parent-title">
                        <?php
                        if ($post->post_parent != 0): ?>
                            <?= $parent ?> >
                        <?php
                        endif; ?>
                    </h3>
                    <h2 class="section-title text-center">
                        <?php
                        if ($dif_title):
                            echo $dif_title;
                        else:
                            the_title();
                        endif;
                        ?>
                    </h2>
                    <?php
                    if ($extra_link) : ?>
                        <div class="hero__link">
                            <a class="btn btn-white" href="<?= $extra_link['url']; ?>"
                               target="<?= $extra_link['target']; ?>">
                                <?= $extra_link['title'] ?>
                            </a>
                        </div>

                    <?php
                    endif; ?>
                </div>
            </div>
        </div>
    </section>


    <section class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <?php
                    the_content();
                    ?>
                </div>
            </div>
        </div>
    </section>

<?php
get_footer();