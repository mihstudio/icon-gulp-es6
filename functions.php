<?php

// Utilities

include( 'configure/utilities.php' );

// CONFIG

include( 'configure/configure.php' );

// JAVASCRIPT & CSS

include( 'configure/js-css.php' );

// SHORTCODES

include( 'configure/shortcodes.php' );

// ACF

include( 'configure/acf.php' );

include( 'configure/custom-type-posts.php' );

include( 'configure/woocommerce.php' );

// HOOKS ADMIN

if( is_admin() )
{
	include( 'configure/admin.php' );
}

